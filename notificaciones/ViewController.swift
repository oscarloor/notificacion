//
//  ViewController.swift
//  notificaciones
//
//  Created by Oscar on 20/7/17.
//  Copyright © 2017 Oscar. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func btnNotificacion(_ sender: UIButton) {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.notificacionPropia()
    }

}

